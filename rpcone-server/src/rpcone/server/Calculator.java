/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rpcone.server;

/**
 *
 * @author ipd
 */
public class Calculator {
    
    public Calculator() {
        System.out.println("Calculator was constructed");
    }    
    
    public double add(double v1, double v2) {
        System.out.println("add() was called");
        return v1 + v2;
    }
    
}
