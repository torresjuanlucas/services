package rpcshout.server;

import java.util.ArrayList;
import rpcshout.pkgint.ShoutInterface;

public class ShoutService implements ShoutInterface {

    private static ArrayList<String> shoutList = new ArrayList<>();
    
    public ShoutService() {
        System.out.println("ShoutService constructor executed");
        
        
        
        
//        database name: rpcshout 
//        user name: rpcshout 
//        password: nNRJbpI2I7wsRRAt
        
        
    }
    
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        
    }
    
    @Override
    public int addShout(String msg) {
        System.out.println("Adding shout: " + msg);
        shoutList.add(msg);
        return 0;
    }

    @Override
    public String[] getAllShouts() {
        System.out.println("Returning list of shouts, count=" + shoutList.size());
        return shoutList.toArray(new String[0]);
    }
    
}
