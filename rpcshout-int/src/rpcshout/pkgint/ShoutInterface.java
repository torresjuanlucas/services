package rpcshout.pkgint;

import java.io.IOException;

public interface ShoutInterface {
    // !!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!!!
    // OUR XML-RPC LIBRARY CAN'T DEAL WITH METHODS RETURNING VOID
    // THIS MUST RETURN INT
    // !!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!!!
    public int addShout(String msg) throws IOException;
    public String[] getAllShouts() throws IOException;
}
